#Atl

An implementation, that tries to mimic what Atlassian's HipChat chat parser would do.

#Building

* You'd need a recent Go compiler. (Tested on Go 1.4+)
* Steps to build
> git clone git@bitbucket.com:prudhvi/atl

> cd atl

> export GOPATH=`pwd`

> go install bitbucket.com/prudhvi/atl

> go test bitbucket.com/prudhvi/atl

> go build bitbicket.com/prudhvi/client

> ./client  
   Press Ctrl-C to exit

# Sample inputs
> ./client

>   > @chris you there?

>   > @bob (aww) check this out; https://www.youtube.com/watch?v=3ZqPaohVjmw

