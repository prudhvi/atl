package main

import (
	"bufio"
	"fmt"
	"os"

	"runtime"

	"bitbucket.com/prudhvi/atl"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("> ")
		text, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			return
		}

		out, err := atl.ProcessInputWithJsonOutput(text)
		if err != nil {
			fmt.Println(err)
			continue
		}

		fmt.Println(out)
	}
}
