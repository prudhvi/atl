package atl

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"runtime"
	"strings"

	"golang.org/x/net/html"
)

//A Type representing a valid emoticon
type Emoticon string

//A Type representing a pair of html title, url
type Link struct {
	Title string `json:title, omitempty`
	Url   string `json:url, omitempty`
}

//A Type representing output of a given chat message string
type StructuredOutput struct {
	Emoticons []Emoticon `json:emoticons, omitempty`
	Mentions  []string   `json:mentions, omitempty`
	Links     []Link     `json:links, omitempty"`
}

//Regular expression reperesenting an emoticon on the inputstring
var EmoticonRegex *regexp.Regexp

//Regular expression for whitespaces
var WhiteSpaceRegex *regexp.Regexp

//Regular expression for an url
var LinkRegex *regexp.Regexp

//Regular expression for an @mention
var MentionRegex *regexp.Regexp

func init() {
	EmoticonRegex = regexp.MustCompile(`\(([[:alpha:]]{1,15})\)`) //regexp.MustCompile(`(\([[:alpha:]]{1,15}\))`)
	WhiteSpaceRegex = regexp.MustCompile(`\s`)
	LinkRegex = regexp.MustCompile(`(http|https):\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?`)
	MentionRegex = regexp.MustCompile(`@(\w+)`)
}

//A helper function to generate a link object from the inputs
func NewLink(title string, url string) Link {
	return Link{Title: title, Url: url}
}

//A helper function to validate if a given string is an emoticon and return an emoticon, error pair
func NewEmoticon(emoticon string) (*Emoticon, error) {
	if emoticon == "" {
		//Not an error. But a sentinel
		return nil, errors.New("No emoticon")
	}

	emtcnstr := EmoticonRegex.FindStringSubmatch(emoticon)

	if len(emtcnstr) == 0 || strings.TrimSpace(emtcnstr[0]) == "" {
		//Not an error. But a sentinel
		return nil, errors.New("No emoticon")
	}

	e := Emoticon(emtcnstr[1])
	return &e, nil

}

//A function to process an array of emoticons and returns array of emoticon objects on the given channel
func processEmoticons(inputChunks []string, ch chan []Emoticon) {
	//var emoticons []Emoticon
	emoticons := []Emoticon{}

	for _, inputstr := range inputChunks {
		emoticon, err := NewEmoticon(inputstr)
		//We'd fail if we aren't able to make a new emoticon object.
		if err == nil {
			emoticons = append(emoticons, *emoticon)
		}
	}

	ch <- emoticons
}

//A function to process any @mentions in the array of inputs and returns such mentions on the given channel
func processMentions(inputChunks []string, ch chan []string) {
	mentions := []string{}

	for _, inputstr := range inputChunks {
		mentionstrs := MentionRegex.FindStringSubmatch(inputstr)

		if len(mentionstrs) > 1 {
			mentions = append(mentions, mentionstrs[1])
		}
	}

	ch <- mentions
}

//A helper function to extract html title information from a given page.
func getPageTitle(resp *http.Response) (string, error) {
	//TODO: Check if robots.txt allows us to query for this resource

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	//Check if the page we are interested in is a valid HTML.
	contentType := resp.Header.Get("Content-Type")

	if !strings.HasPrefix(contentType, "text/html") {

		return "", errors.New("Not a valid html page")
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return "", errors.New("Remote server returned an error")
	}

	bodyReader := strings.NewReader(string(body))

	doc, err := html.Parse(bodyReader)
	if err != nil {
		return "", err
	}

	//A function pointer to a function that parses the html document
	var domParser func(*html.Node, *string)

	var rv string

	domParser = func(n *html.Node, rv *string) {
		if n == nil {
			return
		}
		if n.Type == html.ElementNode && strings.ToLower(n.Data) == "title" {
			//We are only interested in the TITLE object and its data
			*rv = strings.TrimSpace(n.FirstChild.Data)
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			domParser(c, rv)
		}
	}

	domParser(doc, &rv)

	return rv, nil
}

//A function to process all the title information from the links passed in. Returns array of link objects on the channel
func processLinks(inputChunks []string, ch chan []Link) {
	links := []Link{}

	for _, inputstr := range inputChunks {
		link := LinkRegex.FindString(inputstr)
		if link == "" {
			continue
		}

		resp, err := http.Get(link)
		if err != nil {
			continue
		}

		title, err := getPageTitle(resp)
		if title != "" {
			links = append(links, NewLink(title, link))
		}
	}

	ch <- links
}

//A public function that returns a StructuredOutput object
func ProcessInputWithStructuredOutput(input string) *StructuredOutput {
	runtime.GOMAXPROCS(runtime.NumCPU())
	inputChunks := WhiteSpaceRegex.Split(input, -1)

	//Each worker goroutine writes its work output to these channels
	emoticonChan := make(chan []Emoticon)
	linksChan := make(chan []Link)
	mentionsChan := make(chan []string)

	//We launch 3 simultanious goroutines to parallalize work.
	go processEmoticons(inputChunks, emoticonChan)
	go processLinks(inputChunks, linksChan)
	go processMentions(inputChunks, mentionsChan)

	return &StructuredOutput{Emoticons: <-emoticonChan, Mentions: <-mentionsChan, Links: <-linksChan}
}

//A public helper function that returns a StructuredOutput object in JSON
func ProcessInputWithJsonOutput(input string) (string, error) {
	obj := ProcessInputWithStructuredOutput(input)
	objBytes, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}
	return string(objBytes), nil
}
