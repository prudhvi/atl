package atl

import (
	"testing"
)

func TestMentions(t *testing.T) {
	var tests = []struct {
		input    string
		size     int
		mentions []string
	}{
		{"@chris you around?", 1, []string{"chris"}},
		{"@bar @foo updated! @xyz@", 3, []string{"bar", "foo", "xyz"}}, //Negative Test
	}

	for _, test := range tests {
		x := ProcessInputWithStructuredOutput(test.input)
		if test.size != len(x.Mentions) {
			t.Errorf("Expected: Mentions size %d, found: %d", test.size, len(x.Mentions))
		}
		for idx, mention := range test.mentions {
			if mention != x.Mentions[idx] {
				t.Errorf("Expected: Mention: %s, found: %s", mention, x.Mentions[idx])
			}
		}
	}
}

func TestEmoticons(t *testing.T) {
	var tests = []struct {
		input     string
		size      int
		emoticons []Emoticon
	}{
		{"Good morning! (megusta) (coffee)", 2, []Emoticon{Emoticon("megusta"), Emoticon("coffee")}},
		{"Good morning! (megusta) (!coZfee)", 1, []Emoticon{Emoticon("megusta")}}, //Negative Test
	}

	for _, test := range tests {
		x := ProcessInputWithStructuredOutput(test.input)
		if test.size != len(x.Emoticons) {
			t.Errorf("Expected: Emoticons size %d, found: %d", test.size, len(x.Emoticons))
		}

		for idx, emo := range test.emoticons {
			if string(emo) != string(x.Emoticons[idx]) {
				t.Errorf("Expected: Emoticon %s, found: %s", string(emo), string(x.Emoticons[idx]))
			}
		}
	}
}

func TestLinks(t *testing.T) {
	var tests = []struct {
		input string
		size  int
		links []Link
	}{
		{"Olympics are starting soon; http://www.nbcolympics.com", 1, []Link{Link{Title: "NBC Olympics | Home of the 2016 Olympic Games in Rio", Url: "http://www.nbcolympics.com"}}},
	}

	for _, test := range tests {
		x := ProcessInputWithStructuredOutput(test.input)
		if test.size != len(x.Links) {
			t.Errorf("Expected: Links size %d, found: %d", test.size, len(x.Links))
		}

		for idx, link := range test.links {
			if link.Url != x.Links[idx].Url {
				t.Errorf("Expected: URL %s, found: %s", link.Url, x.Links[idx].Url)
			}

			if link.Title != x.Links[idx].Title {
				t.Errorf("Expected: Title: %s, found: %s", link.Title, x.Links[idx].Title)
			}
		}
	}
}

func TestAllExpects(t *testing.T) {
	var tests = []struct {
		input            string
		emoticons_size   int
		links_size       int
		nos_mentions     int
		first_link_title string
	}{
		{"@bob @john (success) (badass) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016", 2, 1, 2, "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""},
		{"@bob @john (success) (badass) (foo) such as cool feature; https://www.google.com", 3, 1, 2, "Google"},
	}

	for _, test := range tests {
		x := ProcessInputWithStructuredOutput(test.input)
		if test.emoticons_size != len(x.Emoticons) {
			t.Errorf("Expected: Emoticons size %d, found: %d", test.emoticons_size, len(x.Emoticons))
		}

		if test.links_size != len(x.Links) {
			t.Errorf("Expected: Links size %d, found: %d", test.links_size, len(x.Links))
		}

		if test.first_link_title != x.Links[0].Title {
			t.Errorf("Expected: Title %s, found: %s", test.first_link_title, x.Links[0].Title)
		}

		if test.nos_mentions != len(x.Mentions) {
			t.Errorf("Expected: Mentions %d, found: %d", test.nos_mentions, len(x.Mentions))
		}
	}
}
